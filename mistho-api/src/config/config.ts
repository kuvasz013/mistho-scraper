import path from 'path';

const rootPath = path.join(__dirname, '..', '..');

const env =
  process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'production'
    ? process.env.NODE_ENV
    : 'development';

const config = {
  default: {
    version: 'v1',
    logLevel: 'debug',

    protocol: 'http',
    listenHost: '127.0.0.1',
    listenPort: 6060,
    hostname: 'localhost',
    getApiUrl: function () {
      return `${this.protocol}://${this.hostname}:${this.listenPort}/api/${this.version}`;
    },

    frontendHostname: 'localhost',
    frontendPort: 3000,

    rootPath,

    enabledOrigins: ['http://localhost:3000', 'http://localhost:6060'],
    sendErrorStack: false,

    glassdoor: {
      loginUrl: 'https://www.glassdoor.com/index.htm',
      signInButtonSelector: '.LockedHomeHeaderStyles__signInButton',
      signInFormSelector: 'form[name="emailSignInForm"]',
      modalEmailSelector: '#modalUserEmail',
      modalPasswordSelector: '#modalUserPassword',
      modalSignInButtonSelector: 'form[name="emailSignInForm"] button[type="submit"]',
      profileUrl: 'https://www.glassdoor.com/member/profile/index.htm?profileOrigin=MEMBER_HOME',
      profileApiUrl: 'https://www.glassdoor.com/member/profileApi/get.htm',
      resumesUrl: 'https://www.glassdoor.com/member/profile/resumes.htm',
      resumeApiUrl: 'https://www.glassdoor.com/member/profileApi/resumeMetaData/getAllUserResumes.htm',
      resumeDownloadBaseUrl: 'https://resume.glassdoor.com/',
      logoutUrl: 'https://www.glassdoor.com/logout.htm',
      resumeDownloadPath: path.join(rootPath, 'data', 'resumes'),
    },
  },
  development: {
    sendErrorStack: true,
  },
  production: {},
};

export default { ...config.default, ...config[env] } as typeof config.default;

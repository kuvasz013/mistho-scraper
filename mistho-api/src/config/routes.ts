import PayrollController from '../modules/Payroll/controllers/PayrollController';
import ResumeController from '../modules/Resume/controllers/ResumeController';

export enum Routes {
  PAYROLL = '/payroll',
  RESUME = '/resume',
}

export default {
  [Routes.PAYROLL]: PayrollController,
  [Routes.RESUME]: ResumeController,
};

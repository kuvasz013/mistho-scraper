import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import config from './config/config';
import routes from './config/routes';
import { StatusCodes } from 'http-status-codes';
import 'reflect-metadata';
import ErrorHandler from './middlewares/ErrorHandling';
import fs from 'fs';

class App {
  public express: Application;

  constructor() {
    this.express = express();
    this.initMiddlewares();
    this.initRoutes();
    this.initErrorHandler();
    this.initDataDir();
  }

  private initRoutes() {
    Object.entries(routes).forEach(([route, controller]) => {
      this.express.use(`/api/${config.version}${route}`, controller);
    });
  }

  private initMiddlewares() {
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: false }));
    this.express.use(morgan('tiny'));
    this.express.use(
      cors({
        origin: config.enabledOrigins,
        methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
        optionsSuccessStatus: StatusCodes.OK,
        exposedHeaders: ['X-Total-Count'],
        credentials: true,
      })
    );
  }

  private initErrorHandler() {
    this.express.use(ErrorHandler);
  }

  private initDataDir() {
    const init = async () => {
      await fs.promises.access(config.glassdoor.resumeDownloadPath, fs.constants.F_OK).catch(async (e) => {
        console.warn(`Directory does not exist: ${config.glassdoor.resumeDownloadPath}. Creating directory...`);
        await fs.promises.mkdir(config.glassdoor.resumeDownloadPath, { recursive: true });
      });
    };
    init();
  }
}

export default new App().express;

import { DataSource, Like } from 'typeorm';
import { Education } from './modules/Payroll/models/Education';
import { Employment } from './modules/Payroll/models/Employment';
import { License } from './modules/Payroll/models/License';
import { MisthoFile } from './modules/Payroll/models/MisthoFile';
import { Organization } from './modules/Payroll/models/Organization';
import { Payroll } from './modules/Payroll/models/Payroll';

const entities = [Payroll, Education, Employment, License, Organization, MisthoFile];

const PgDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  database: 'mistho',
  username: 'mistho',
  password: 'super-secret-password',
  synchronize: true,
  logging: ['error'],
  entities,
  subscribers: [],
  migrations: [],
});

export default PgDataSource;

import { Router } from 'express';
import TypedHandle from '../../../middlewares/TypedHandle';
import { MisthoFile } from '../../Payroll/models/MisthoFile';

const router = Router();

router.get(
  '/:id',
  TypedHandle<void, void>(async (req, res) => {
    const { id } = req.params;
    const file = await MisthoFile.findOneByOrFail({ id });
    return res.download(file.filePath, file.fileName);
  })
);

export default router;

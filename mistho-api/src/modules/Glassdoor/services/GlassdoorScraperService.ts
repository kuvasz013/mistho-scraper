import axios from 'axios';
import { randomUUID } from 'crypto';
import { createWriteStream } from 'fs';
import path from 'path';
import puppeteer, { Page } from 'puppeteer';
import Logger from '../../../common/Logger';
import config from '../../../config/config';
import routes, { Routes } from '../../../config/routes';
import { Education } from '../../Payroll/models/Education';
import { Employment } from '../../Payroll/models/Employment';
import { License } from '../../Payroll/models/License';
import { MisthoFile } from '../../Payroll/models/MisthoFile';
import { Organization } from '../../Payroll/models/Organization';
import { Payroll } from '../../Payroll/models/Payroll';
import { GlassdoorResumeDto } from '../types/GlassdoorResumeDto';
import GlassdoorProfileSection from '../enum/GlassdoorProfileSection';
import { BadRequestException as BadRequestException } from '../../../common/errors/BadRequestException';
import { UnauthorizedException } from '../../../common/errors/UnauthorizedException';

const logger = Logger(__filename);

class GlassdoorScraperService {
  async scrape(email: string, password: string): Promise<Payroll> {
    logger.info(`Scraping Payroll from Glassdoor for: ${email}`);

    // Cannot circumvent CSRF, load page with Puppeteer for login
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Skip loading unnecessary files
    page.setRequestInterception(true);
    page.on('request', (request) => {
      if (['image', 'stylesheet', 'font'].indexOf(request.resourceType()) !== -1) {
        request.abort();
      } else {
        request.continue();
      }
    });

    logger.info(`Navigating to Glassdoor page for: ${email}`);
    await page.goto(config.glassdoor.loginUrl);
    await Promise.all([
      page.click(config.glassdoor.signInButtonSelector),
      page.waitForSelector(config.glassdoor.signInFormSelector),
    ]);

    // Entering login info
    logger.info(`Entering login info for: ${email}`);
    await page.type(config.glassdoor.modalEmailSelector, email);
    await page.type(config.glassdoor.modalPasswordSelector, password);
    await Promise.all([page.click(config.glassdoor.modalSignInButtonSelector), page.waitForNavigation()]).catch((e) => {
      throw new UnauthorizedException('Failed to authenticate to Glassdoor server');
    });

    // Create a Promise which resolves when all profile sections have been processed
    logger.info(`Processing Profile page for: ${email}`);
    const payroll = new Payroll();
    payroll.email = email;
    const sections = Object.values(GlassdoorProfileSection);
    const sectionPromises = Promise.all(sections.map((section) => this.waitForSection(page, section, payroll)));

    await page.goto(config.glassdoor.profileUrl);
    await sectionPromises;

    logger.info(`Downloading resume for: ${email}`);
    const resume = await this.downloadResume(page, payroll);
    payroll.resumeUrl = `${config.getApiUrl()}${Routes.RESUME}/${resume.id}`;

    logger.info(`Logging out and closing browser for: ${email}`);
    await page.goto(config.glassdoor.logoutUrl);

    await payroll.save();
    await resume.save();
    // This could use some error handling :)

    return payroll;
  }

  private waitForSection = async (page: Page, sectionToProcess: GlassdoorProfileSection, payroll: Payroll) => {
    await page.waitForResponse(async (res) => {
      if (res.url() !== config.glassdoor.profileApiUrl) return false;

      const response = await res.json();
      const section = Object.keys(response)[0];

      // Resolve only for a specific section
      if (section === sectionToProcess) {
        await this.processSection[sectionToProcess](response[section], payroll);
        return true;
      } else return false;
    });
  };

  private processSection: Record<GlassdoorProfileSection, (data: any, payroll: Payroll) => Promise<void>> = {
    [GlassdoorProfileSection.CONTACT_INFO]: async (data, payroll) => {
      payroll.phoneNumber = data.phone;
    },

    [GlassdoorProfileSection.SKILLS]: async (data, payroll) => {
      payroll.skills = data.map((item: { skill: string }) => item.skill);
    },

    [GlassdoorProfileSection.EDUCATION]: async (data, payroll) => {
      payroll.educations = await Promise.all(
        data.map(async (e: any) => {
          const education = new Education();
          education.degree = e.degreeName;
          education.field = e.field;
          education.location = e.location;
          education.description = e.description;
          education.ongoing = e.presentlyEnrolled;

          education.startYear = e.startYear;
          education.startMonth = e.startMonth;
          education.endYear = e.endYear;
          education.endMonth = e.endMonth;

          const organization = await Organization.findOneBy({ name: e.schoolName });
          if (organization) {
            education.organization = organization;
          } else {
            education.organization = new Organization();
            education.organization.name = e.schoolName;
          }

          return education;
        })
      );
    },

    [GlassdoorProfileSection.EXPERIENCE]: async (data, payroll) => {
      payroll.employments = await Promise.all(
        data.map(async (e: any) => {
          const employment = new Employment();
          employment.title = e.title;
          employment.location = e.location;
          employment.description = e.description;
          employment.ongoing = e.currentPosition;

          employment.startYear = e.startYear;
          employment.startMonth = e.startMonth;
          employment.endYear = e.endYear;
          employment.endMonth = e.endMonth;

          const organization = await Organization.findOneBy({ name: e.employerName });
          if (organization) {
            employment.organization = organization;
          } else {
            employment.organization = new Organization();
            employment.organization.name = e.employerName;
            employment.organization.logoUrl = e.squareLogoUrl;
          }

          return employment;
        })
      );
    },

    [GlassdoorProfileSection.WEBSITE]: async (data, payroll) => {
      payroll.websites = data.map((item: any) => item.url);
    },

    [GlassdoorProfileSection.ABOUT_ME]: async (data, payroll) => {
      payroll.introduction = data.description;
    },

    [GlassdoorProfileSection.CERTIFICATION]: async (data, payroll) => {
      payroll.licenses = await Promise.all(
        data.map(async (e: any) => {
          const license = new License();
          license.title = e.title;
          license.description = e.description;
          license.neverExpires = e.endYear && e.currentlyValid;

          license.issueYear = e.startYear;
          license.issueMonth = e.startMonth;
          license.expiratonYear = e.endYear;
          license.expirationMonth = e.endMonth;

          const organization = await Organization.findOneBy({ name: e.userEnteredIssuer });
          if (organization) {
            license.organization = organization;
          } else {
            license.organization = new Organization();
            license.organization.name = e.userEnteredIssuer;
            license.organization.logoUrl = e.squareLogoUrl;
          }

          return license;
        })
      );
    },

    [GlassdoorProfileSection.PROFILE_HEADER]: async (data, payroll) => {
      payroll.firstname = data.fname;
      payroll.lastname = data.lname;
      payroll.title = data.headline;
      payroll.location = data.location;
      payroll.profileImgUrl = data.profilePhotoUrl;
    },
  };

  private downloadResume = async (page: Page, payroll: Payroll): Promise<MisthoFile> => {
    // Cheating the compiler here to access the private field of Puppeteer :/
    // This seems to be the only way to set the download path
    await (page as any)._client.send('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: config.glassdoor.resumeDownloadPath,
    });

    const file = new MisthoFile();
    file.id = randomUUID();
    file.email = payroll.email!;

    await page.goto(config.glassdoor.resumesUrl);
    await page.waitForResponse(async (res) => {
      if (res.url() !== config.glassdoor.resumeApiUrl) return false;

      const [resume]: GlassdoorResumeDto[] = await res.json();

      // Multiple files with the same name not handled in POC
      file.fileName = resume.originalFileName;
      file.filePath = path.join(config.glassdoor.resumeDownloadPath, file.fileName);
      file.mimeType = resume.contentType;
      return true;
    });

    await page.click('.resumeFileName a');
    return file;
  };
}

const instance = new GlassdoorScraperService();

export default instance;

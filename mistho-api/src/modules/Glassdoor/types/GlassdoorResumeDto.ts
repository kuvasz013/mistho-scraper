export type GlassdoorResumeDto = {
  originalFileName: string;
  contentType: string;
};

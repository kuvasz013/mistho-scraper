import { Router } from 'express';
import { StatusCodes } from 'http-status-codes';
import PayrollService from '../services/PayrollService';
import { Payroll } from '../models/Payroll';
import TypedHandle from '../../../middlewares/TypedHandle';
import { GetPayrollDto } from '../types/GetPayrollDto';

const router = Router();

router.post(
  '/',
  TypedHandle<GetPayrollDto, Payroll>(async (req, res) => {
    const payroll = await PayrollService.getPayroll(req.body);
    return res.status(StatusCodes.OK).send(payroll);
  })
);

export default router;

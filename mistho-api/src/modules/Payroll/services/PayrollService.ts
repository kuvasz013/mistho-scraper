import { BadRequestException } from '../../../common/errors/BadRequestException';
import Logger from '../../../common/Logger';
import GlassdoorScraperService from '../../Glassdoor/services/GlassdoorScraperService';
import { Payroll } from '../models/Payroll';
import { GetPayrollDto } from '../types/GetPayrollDto';

const logger = Logger(__filename);

class PayrollService {
  async getPayroll({ email, password }: GetPayrollDto): Promise<Payroll> {
    if (!email || !password) {
      throw new BadRequestException('Email and Password fields are required!');
    }

    const savedPayroll = await Payroll.getRepository()
      .createQueryBuilder('payroll')
      .innerJoinAndSelect('payroll.employments', 'employment')
      .innerJoinAndSelect('employment.organization', 'org1')

      .innerJoinAndSelect('payroll.educations', 'education')
      .innerJoinAndSelect('education.organization', 'org2')

      .innerJoinAndSelect('payroll.licenses', 'license')
      .innerJoinAndSelect('license.organization', 'org3')

      .where('payroll.email = :email', { email })
      .getOne();

    if (savedPayroll) {
      return savedPayroll;
    } else {
      return GlassdoorScraperService.scrape(email, password);
    }
  }
}

const instance = new PayrollService();

export default instance;

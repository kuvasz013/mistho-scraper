import { Column, Entity, OneToMany } from 'typeorm';
import ModelBase from './ModelBase';

@Entity()
export class MisthoFile extends ModelBase {
  @Column()
  filePath: string;

  @Column()
  fileName: string;

  @Column()
  mimeType: string;

  @Column()
  email: string;
}

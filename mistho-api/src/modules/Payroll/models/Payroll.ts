import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Education } from './Education';
import { Employment } from './Employment';
import { License } from './License';
import ModelBase from './ModelBase';

@Entity()
export class Payroll extends ModelBase {
  @Column({ nullable: true })
  firstname?: string;

  @Column({ nullable: true })
  lastname?: string;

  @Column({ nullable: true })
  title?: string;

  @Column()
  email: string;

  @Column('varchar', { array: true, default: [], nullable: false })
  websites: string[];

  @Column({ nullable: true })
  phoneNumber?: string;

  @Column({ nullable: true })
  location?: string;

  @Column({ nullable: true })
  profileImgUrl?: string;

  @Column({ nullable: true })
  introduction?: string;

  @Column('varchar', { array: true, default: [], nullable: false })
  skills: string[];

  @Column({ nullable: true })
  resumeUrl?: string;

  @OneToMany(() => Employment, (employment) => employment.payroll, { cascade: true })
  employments: Employment[];

  @OneToMany(() => Education, (education) => education.payroll, { cascade: true })
  educations: Education[];

  @OneToMany(() => License, (license) => license.payroll, { cascade: true })
  licenses: License[];
}

import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Education } from './Education';
import { Employment } from './Employment';
import { License } from './License';
import ModelBase from './ModelBase';

@Entity()
export class Organization extends ModelBase {
  @Column()
  name: string;

  @Column({ nullable: true })
  logoUrl?: string;

  @OneToMany(() => Employment, (employment) => employment.organization)
  employments: Employment[];

  @OneToMany(() => Education, (education) => education.organization)
  educations: Education[];

  @OneToMany(() => License, (license) => license.organization)
  licenses: License[];
}

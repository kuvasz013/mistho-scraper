import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import ModelBase from './ModelBase';
import { Organization } from './Organization';
import { Payroll } from './Payroll';

@Entity()
export class License extends ModelBase {
  @Column()
  title: string;

  @Column({ nullable: true })
  issueYear?: number;

  @Column({ nullable: true })
  issueMonth?: number;

  @Column({ nullable: true })
  expiratonYear?: number;

  @Column({ nullable: true })
  expirationMonth?: number;

  @Column({ nullable: true })
  neverExpires?: boolean;

  @Column({ nullable: true })
  description?: string;

  @ManyToOne(() => Organization, (org) => org.licenses, { onDelete: 'SET NULL', cascade: true })
  organization?: Organization;

  @ManyToOne(() => Payroll, (payroll) => payroll.licenses, { onDelete: 'CASCADE' })
  payroll: Payroll;
}

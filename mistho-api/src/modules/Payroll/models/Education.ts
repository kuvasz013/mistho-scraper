import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import ModelBase from './ModelBase';
import { Organization } from './Organization';
import { Payroll } from './Payroll';

@Entity()
export class Education extends ModelBase {
  @Column()
  degree: string;

  @Column({ nullable: true })
  field?: string;

  @Column({ nullable: true })
  location?: string;

  @Column({ nullable: true })
  startYear?: number;

  @Column({ nullable: true })
  startMonth?: number;

  @Column({ nullable: true })
  endYear?: number;

  @Column({ nullable: true })
  endMonth?: number;

  @Column({ nullable: true })
  ongoing?: boolean;

  @Column({ nullable: true })
  description?: string;

  @ManyToOne(() => Organization, (org) => org.educations, { onDelete: 'SET NULL', cascade: true })
  organization?: Organization;

  @ManyToOne(() => Payroll, (payroll) => payroll.educations, { onDelete: 'CASCADE' })
  payroll: Payroll;
}

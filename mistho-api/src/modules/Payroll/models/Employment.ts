import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import ModelBase from './ModelBase';
import { Organization } from './Organization';
import { Payroll } from './Payroll';

@Entity()
export class Employment extends ModelBase {
  @Column()
  title: string;

  @Column({ nullable: true })
  location?: string;

  @Column()
  startYear: number;

  @Column({ nullable: true })
  startMonth?: number;

  @Column({ nullable: true })
  endYear?: number;

  @Column({ nullable: true })
  endMonth?: number;

  @Column({ nullable: true })
  ongoing?: boolean;

  @Column({ nullable: true })
  description?: string;

  @ManyToOne(() => Organization, (org) => org.employments, { onDelete: 'SET NULL', cascade: true })
  organization?: Organization;

  @ManyToOne(() => Payroll, (payroll) => payroll.employments, { onDelete: 'CASCADE' })
  payroll: Payroll;
}

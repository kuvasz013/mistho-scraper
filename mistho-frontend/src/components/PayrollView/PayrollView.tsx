import { Payroll } from '../../models/Payroll';
import ExperienceItem from '../ExperienceItem/ExperienceItem';
import './PayrollView.css';

type Props = {
  payroll: Payroll;
};

const PayrollView = ({ payroll }: Props) => {
  return (
    <div className='Payroll-wrapper'>
      <section className='Payroll-header'>
        <img className='Payroll-image' src={payroll.profileImgUrl} />

        <div className='Payroll-main-container'>
          <div className='Payroll-about-container'>
            <h1 className='Payroll-name'>
              {payroll.firstname} {payroll.lastname}
            </h1>
            <span className='Payroll-title'>{payroll.title}</span>
            <span>{payroll.email}</span>
            <span>{payroll.location}</span>
            <span>{payroll.phoneNumber}</span>
          </div>
          <div className='Payroll-websites'>
            {payroll.websites.map((url, index) => (
              <a key={index} href={url} target='_blank'>
                {url}
              </a>
            ))}
          </div>
        </div>
        <div className='Payroll-button-container'>
          <a onClick={() => (location.href = payroll.resumeUrl)}>Download Resume PDF</a>
        </div>
      </section>

      <section className='Payroll-intro'>
        <p>{payroll.introduction}</p>
      </section>

      <section className='Payroll-skill-container'>
        {payroll.skills?.map((skill, index) => (
          <div key={index} className='Payroll-skill-badge'>
            {skill}
          </div>
        ))}
      </section>

      <section className='Payroll-section'>
        <h1 className='Payroll-subheader'>Employment</h1>
        {payroll.employments?.map((employment, index) => (
          <ExperienceItem
            key={index}
            title={employment.title}
            logoUrl={employment.organization?.logoUrl}
            organization={employment.organization?.name}
            location={employment.location}
            startYear={employment.startYear}
            startMonth={employment.startMonth}
            endYear={employment.endYear}
            endMonth={employment.endMonth}
            description={employment.description}
          />
        ))}
      </section>

      <section className='Payroll-section'>
        <h1 className='Payroll-subheader'>Education</h1>
        {payroll.educations?.map((education, index) => (
          <ExperienceItem
            key={index}
            title={education.degree}
            logoUrl={education.organization?.logoUrl}
            organization={education.organization?.name}
            location={education.location}
            startYear={education.startYear}
            startMonth={education.startMonth}
            endYear={education.endYear}
            endMonth={education.endMonth}
            description={education.description}
          />
        ))}
      </section>

      <section className='Payroll-section'>
        <h1 className='Payroll-subheader'>{'Licenses & Certificates'}</h1>
        {payroll.licenses?.map((license, index) => (
          <ExperienceItem
            key={index}
            title={license.title}
            logoUrl={license.organization?.logoUrl}
            organization={license.organization?.name}
            startYear={license.issueYear}
            startMonth={license.issueMonth}
            endYear={license.expiratonYear}
            endMonth={license.expirationMonth}
            description={license.description}
          />
        ))}
      </section>
    </div>
  );
};

export default PayrollView;

import { useMemo } from 'react';
import './ExperienceItem.css';

type Props = {
  title: string;
  logoUrl?: string;
  organization?: string;
  location?: string;
  startYear?: number;
  startMonth?: number;
  endYear?: number;
  endMonth?: number;
  description?: string;
};

const ExperienceItem = (props: Props) => {
  const timelineString = useMemo(() => {
    let timestring = '';
    if (props.startMonth) timestring += months[props.startMonth - 1] + ' ';
    if (props.startYear) timestring += props.startYear + ' ';
    if (timestring.length) timestring += '- ';
    if (props.endMonth) timestring += months[props.endMonth - 1] + ' ';
    if (props.endYear) timestring += props.endYear;
    return timestring;
  }, [props.startMonth, props.startYear, props.endMonth, props.endYear]);

  return (
    <div className='Exp-item'>
      {props.logoUrl ? (
        <img src={props.logoUrl} className='Exp-logo' style={{ backgroundImage: props.logoUrl }} />
      ) : (
        <div className='Exp-logo Exp-logo-default'>{props.organization?.charAt(0)}</div>
      )}
      <div className='Exp-text-block'>
        <h1>{props.title}</h1>
        <h2>{props.organization}</h2>
        <p>{props.location}</p>
        <p>{timelineString}</p>
        <p className='Exp-description'>{props.description}</p>
      </div>
    </div>
  );
};

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export default ExperienceItem;

import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';
import './Login.css';

type Props = {
  onLogin: (email: string, password: string) => void;
};

function Login({ onLogin }: Props) {
  const [email, setEmail] = useState<string>('ravi.van.test@gmail.com');
  const [password, setPassword] = useState<string>('ravi.van.test@gmail.com');

  const handleSubmit = useCallback(
    (email: string, password: string) => {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) && password && password.length) {
        onLogin(email, password);
      } else {
        toast('Invalid credentials', { type: 'error' });
      }
    },
    [onLogin]
  );

  return (
    <div className='Login-wrapper'>
      <div className='Login-header'>
        <img src='mistho_logo_full.svg' className='Login-logo' />
        <p className='Login-title'>MyScrape Login</p>
      </div>
      <form className='Login-form'>
        <div className='Login-input-container'>
          <span className='Login-label'>Email</span>
          <input required type='email' value={email} onChange={(e) => setEmail(e.target.value)} />
        </div>
        <div className='Login-input-container'>
          <span className='Login-label'>Password</span>
          <input required type='password' value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
      </form>
      <button className='Login-button' onClick={() => handleSubmit(email, password)}>
        Sign in
      </button>
      <div className='Login-text'>
        <span>After sign in, scraping the web might take a minute</span>
      </div>
    </div>
  );
}

export default Login;

import { useCallback, useState } from 'react';
import { Payroll } from '../../models/Payroll';
import Login from '../Login/Login';
import { PacmanLoader } from 'react-spinners';
import axios from 'axios';
import config from '../../config';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import PayrollView from '../PayrollView/PayrollView';

function App() {
  const [payroll, setPayroll] = useState<Payroll>();
  const [isScraping, setScraping] = useState<boolean>(false);

  const getPayroll = useCallback(async (email: string, password: string) => {
    setScraping(true);

    await axios
      .post<Payroll>(`${config.apiUrl}/payroll`, { email, password })
      .then((response) => {
        if (response.status === 200) setPayroll(response.data);
      })
      .catch((err) => {
        console.error(err?.response?.data);
        toast(err?.response?.data?.message ?? 'Unknown error, please try again later', { type: 'error' });
      });

    setScraping(false);
  }, []);

  return (
    <div className='App'>
      <ToastContainer autoClose={3000} />
      <header className='App-header'>
        <div className='App-header-container' onClick={() => location.reload()}>
          <img src='mistho_logo.svg' className='App-logo' />
          <h1 className='App-title'>MyScrape</h1>
        </div>
      </header>
      <main>
        <div className='App-loader-container' hidden={!isScraping}>
          <PacmanLoader color='#00fff8' />
        </div>

        <div hidden={isScraping}>{payroll ? <PayrollView payroll={payroll} /> : <Login onLogin={getPayroll} />}</div>
      </main>
    </div>
  );
}

export default App;

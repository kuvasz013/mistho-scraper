import { Education } from './Education';
import { Employment } from './Employment';
import { License } from './License';

export type Organization = {
  name: string;
  logoUrl?: string;
};

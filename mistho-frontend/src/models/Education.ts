import { Organization } from './Organization';
import { Payroll } from './Payroll';

export type Education = {
  degree: string;
  field?: string;
  location?: string;
  startYear: number;
  startMonth?: number;
  endYear?: number;
  endMonth?: number;
  ongoing?: boolean;
  description?: string;
  organization?: Organization;
};

import { Organization } from './Organization';
import { Payroll } from './Payroll';

export type Employment = {
  title: string;
  location?: string;
  startYear: number;
  startMonth?: number;
  endYear?: number;
  endMonth?: number;
  ongoing?: boolean;
  description?: string;
  organization?: Organization;
};

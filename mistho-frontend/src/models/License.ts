import { Organization } from './Organization';
import { Payroll } from './Payroll';

export type License = {
  title: string;
  issueYear?: number;
  issueMonth?: number;
  expiratonYear?: number;
  expirationMonth?: number;
  neverExpires?: boolean;
  description?: string;
  organization?: Organization;
};

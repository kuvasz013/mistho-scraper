import { Education } from './Education';
import { Employment } from './Employment';
import { License } from './License';

export type Payroll = {
  firstname?: string;
  lastname?: string;
  title?: string;
  email: string;
  websites: string[];
  phoneNumber?: string;
  location?: string;
  profileImgUrl?: string;
  introduction?: string;
  skills: string[];
  resumeUrl: string;
  employments: Employment[];
  educations: Education[];
  licenses: License[];
};

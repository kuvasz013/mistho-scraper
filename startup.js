const concurrently = require('concurrently');

const apps = {
  api: './mistho-api',
  frontend: './mistho-frontend',
};

concurrently([
  {
    name: 'API',
    command: `yarn --cwd ${apps.api} start:dev`,
    prefixColor: 'bgBlue.bold',
  },
  {
    name: 'FRONTEND',
    command: `yarn --cwd ${apps.frontend} dev`,
    prefixColor: 'bgMagenta.bold',
  },
]);
